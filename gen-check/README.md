## Generator checks
### How to check
##### Before running the test
- Put the [`porth-cpypst`](https://gitlab.com/oxydemeton/porth-cpypst/-/tree/main/) (name can be changed) into your folder with the [`porth`](https://gitlab.com/tsoding/porth) executable and the [`std`](https://gitlab.com/tsoding/porth/-/tree/master/std) folder.
- Let the [`gen-check`](https://gitlab.com/oxydemeton/porth-cpypst/-/tree/main/gen-check) (name can be changed) into the [`porth-cpypst`](https://gitlab.com/oxydemeton/porth-cpypst/-/tree/main/) (name can be changed) folder.
##### Run the following commands and check the output.
```console
$ chmod +x test.sh
$ ./test.sh
```
If the porth compiler gives Errors or something doesn't match the following result the cpypst.porth doesn't work as intended.
~~~
Trying com.porth
Pleas check if the compilation gives errors. 

Compilation DONE
Removed com.asm and com itself again

Tryig order.porth
Pleas Check if the order of the Elements in the parts keeps the same and watch out for Error msg

Part 1: 
10
20
30
40
Part 1: check: 
10
20
30
40
Part 1: In code check correct.
Part 2:
"Test string"
0
Part 2 check:
"Test string"
0
Part 2: In code check correct.

Compilation & execution DONE
Removed order.asm and com itself again
~~~
