include "core.porth"

//Buffer Allocation
const cpypst_buf_size 8 4 * end
memory cpypst_buf cpypst_buf_size end

//Proc to clear the buffer.
proc cpypst_free in 0 while dup cpypst_buf_size < do dup cpypst_buf cast(int) + cast(ptr) 0 swap !8 1 + end drop end
inline proc cpypst_buf_byte int -- ptr in cpypst_buf swap ptr+ end

//Consts for multiplication of sizeof()
const sizeof(int)2 sizeof(int) 2 * end
const sizeof(int)3 sizeof(int) 3 * end
const sizeof(bool)2 sizeof(bool) 2 * end
const sizeof(bool)3 sizeof(bool) 3 * end
const sizeof(ptr)2 sizeof(ptr) 2 * end
const sizeof(ptr)3 sizeof(ptr) 3 * end
const sizeof(addr)2 sizeof(addr) 2 * end
const sizeof(addr)3 sizeof(addr) 3 * end

const sizeof(int+int) sizeof(int) sizeof(int) + end
const sizeof(int+bool) sizeof(int) sizeof(bool) + end
const sizeof(int+ptr) sizeof(int) sizeof(ptr) + end
const sizeof(int+addr) sizeof(int) sizeof(addr) + end
const sizeof(bool+int) sizeof(bool) sizeof(int) + end
const sizeof(bool+bool) sizeof(bool) sizeof(bool) + end
const sizeof(bool+ptr) sizeof(bool) sizeof(ptr) + end
const sizeof(bool+addr) sizeof(bool) sizeof(addr) + end
const sizeof(ptr+int) sizeof(ptr) sizeof(int) + end
const sizeof(ptr+bool) sizeof(ptr) sizeof(bool) + end
const sizeof(ptr+ptr) sizeof(ptr) sizeof(ptr) + end
const sizeof(ptr+addr) sizeof(ptr) sizeof(addr) + end
const sizeof(addr+int) sizeof(addr) sizeof(int) + end
const sizeof(addr+bool) sizeof(addr) sizeof(bool) + end
const sizeof(addr+ptr) sizeof(addr) sizeof(ptr) + end
const sizeof(addr+addr) sizeof(addr) sizeof(addr) + end


// Macros wich can be dynamicly included by using unimacro.py from https://gitlab.com/oxydemeton/unimacros

// %%%cut_1; cast(int) cut_i
// %%%copy_1; dup cast(int) cut_i
// %%%cut_2; swap cast(int) swap cast(int) cut_ii
// %%%copy_2; over cast(int) over cast(int) cut_ii
// %%%cut_3; let a b c in a cast(int) b cast(int) c cast(int) cut_iii end
// %%%copy_3; let a b c in a cast(int) b cast(int) c cast(int) cut_iii  end
// %%%cut_4; let a b c d in a cast(int) b cast(int) c cast(int) d cast(int) cut_iiii end
// %%%copy_4; peek a b c d in a cast(int) b cast(int) c cast(int) d cast(int) cut_iiii end

//Procs to cut a single Element
proc cut_i int in cpypst_free cpypst_buf !int end
proc cut_b bool in cpypst_free cpypst_buf !bool end
proc cut_p ptr in cpypst_free cpypst_buf !ptr end
proc cut_a addr in cpypst_free cpypst_buf !addr end

 //Procs to cut two Elements
proc cut_ii int int in cpypst_free swap cpypst_buf !int cpypst_buf sizeof(int) ptr+ !int end
proc cut_ib int bool in cpypst_free swap cpypst_buf !int cpypst_buf sizeof(int) ptr+ !bool end
proc cut_ip int ptr in cpypst_free swap cpypst_buf !int cpypst_buf sizeof(int) ptr+ !ptr end
proc cut_ia int addr in cpypst_free swap cpypst_buf !int cpypst_buf sizeof(int) ptr+ !addr end
proc cut_bi bool int in cpypst_free swap cpypst_buf !bool cpypst_buf sizeof(bool) ptr+ !int end
proc cut_bb bool bool in cpypst_free swap cpypst_buf !bool cpypst_buf sizeof(bool) ptr+ !bool end
proc cut_bp bool ptr in cpypst_free swap cpypst_buf !bool cpypst_buf sizeof(bool) ptr+ !ptr end
proc cut_ba bool addr in cpypst_free swap cpypst_buf !bool cpypst_buf sizeof(bool) ptr+ !addr end
proc cut_pi ptr int in cpypst_free swap cpypst_buf !ptr cpypst_buf sizeof(ptr) ptr+ !int end
proc cut_pb ptr bool in cpypst_free swap cpypst_buf !ptr cpypst_buf sizeof(ptr) ptr+ !bool end
proc cut_pp ptr ptr in cpypst_free swap cpypst_buf !ptr cpypst_buf sizeof(ptr) ptr+ !ptr end
proc cut_pa ptr addr in cpypst_free swap cpypst_buf !ptr cpypst_buf sizeof(ptr) ptr+ !addr end
proc cut_ai addr int in cpypst_free swap cpypst_buf !addr cpypst_buf sizeof(addr) ptr+ !int end
proc cut_ab addr bool in cpypst_free swap cpypst_buf !addr cpypst_buf sizeof(addr) ptr+ !bool end
proc cut_ap addr ptr in cpypst_free swap cpypst_buf !addr cpypst_buf sizeof(addr) ptr+ !ptr end
proc cut_aa addr addr in cpypst_free swap cpypst_buf !addr cpypst_buf sizeof(addr) ptr+ !addr end

// Proc to cut 3 ints
proc cut_iii int int int in cpypst_free let a b c in a cpypst_buf !int  b cpypst_buf sizeof(int) ptr+ !int c cpypst_buf sizeof(int)2 ptr+ !int end end

// Proc to cut 4 ints
proc cut_iiii int int int int in cpypst_free let a b c d in a cpypst_buf !int b cpypst_buf sizeof(int) ptr+ !int c cpypst_buf sizeof(int)2 ptr+ !int d cpypst_buf  sizeof(int)3 ptr+ !int end end

//Procs to copy a single Element
inline proc copy_i int -- int in dup cut_i end
inline proc copy_b bool -- bool in dup cut_b end
inline proc copy_p ptr -- ptr in dup cut_p end
inline proc copy_a addr -- addr in dup cut_a end

//Proc to copy two Elements
inline proc copy_ii int int -- int int in over over cut_ii end
inline proc copy_ib int bool -- int bool in over over cut_ib end
inline proc copy_ip int ptr -- int ptr in over over cut_ip end
inline proc copy_ia int addr -- int addr in over over cut_ia end
inline proc copy_bi bool int -- bool int in over over cut_bi end
inline proc copy_bb bool bool -- bool bool in over over cut_bb end
inline proc copy_bp bool ptr -- bool ptr in over over cut_bp end
inline proc copy_ba bool addr -- bool addr in over over cut_ba end
inline proc copy_pi ptr int -- ptr int in over over cut_pi end
inline proc copy_pb ptr bool -- ptr bool in over over cut_pb end
inline proc copy_pp ptr ptr -- ptr ptr in over over cut_pp end
inline proc copy_pa ptr addr -- ptr addr in over over cut_pa end
inline proc copy_ai addr int -- addr int in over over cut_ai end
inline proc copy_ab addr bool -- addr bool in over over cut_ab end
inline proc copy_ap addr ptr -- addr ptr in over over cut_ap end
inline proc copy_aa addr addr -- addr addr in over over cut_aa end

// Proc to copy 3 ints
inline proc copy_iii int int int -- int int int in peek a b c in a b c cut_iii end end
// Proc to copy 4 ints
inline proc copy_iiii int int int int -- int int int int in peek a b c d in a b c d cut_iiii end end

 //Procs to paste a single Element
inline proc paste_i -- int in cpypst_buf @int end
inline proc paste_b -- bool in cpypst_buf @bool end
inline proc paste_p -- ptr in cpypst_buf @ptr end
inline proc paste_a -- addr in cpypst_buf @addr end

 //Procs to paste two Element
inline proc paste_ii -- int int in paste_i sizeof(int)  cpypst_buf_byte @int end
inline proc paste_ib -- int bool in paste_i sizeof(int)  cpypst_buf_byte @bool end
inline proc paste_ip -- int ptr in paste_i sizeof(int)  cpypst_buf_byte @ptr end
inline proc paste_ia -- int addr in paste_i sizeof(int)  cpypst_buf_byte @addr end
inline proc paste_bi -- bool int in paste_b sizeof(bool)  cpypst_buf_byte @int end
inline proc paste_bb -- bool bool in paste_b sizeof(bool)  cpypst_buf_byte @bool end
inline proc paste_bp -- bool ptr in paste_b sizeof(bool)  cpypst_buf_byte @ptr end
inline proc paste_ba -- bool addr in paste_b sizeof(bool)  cpypst_buf_byte @addr end
inline proc paste_pi -- ptr int in paste_p sizeof(ptr)  cpypst_buf_byte @int end
inline proc paste_pb -- ptr bool in paste_p sizeof(ptr)  cpypst_buf_byte @bool end
inline proc paste_pp -- ptr ptr in paste_p sizeof(ptr)  cpypst_buf_byte @ptr end
inline proc paste_pa -- ptr addr in paste_p sizeof(ptr)  cpypst_buf_byte @addr end
inline proc paste_ai -- addr int in paste_a sizeof(addr)  cpypst_buf_byte @int end
inline proc paste_ab -- addr bool in paste_a sizeof(addr)  cpypst_buf_byte @bool end
inline proc paste_ap -- addr ptr in paste_a sizeof(addr)  cpypst_buf_byte @ptr end
inline proc paste_aa -- addr addr in paste_a sizeof(addr)  cpypst_buf_byte @addr end

// Proc to paste 3 ints
inline proc paste_iii -- int int int in paste_ii sizeof(int)2 cpypst_buf_byte @int end

// Proc to paste 4 ints
proc paste_iiii -- int int int int in cpypst_buf @int sizeof(int) cpypst_buf_byte @int sizeof(int)2 cpypst_buf_byte @int sizeof(int)3 cpypst_buf_byte @int end

// Proc to cut a string
inline proc cut_s int ptr in cut_ip end

// Proc to copy a string
inline proc copy_s int ptr -- int ptr in over over cut_s end

// Proc to paste a string
inline proc paste_s -- int ptr in paste_ip end

// Proc to cut two string
inline proc cut_ss int ptr int ptr in let n1 s1 n2 s2 in n1 s1 cast(int) n2 s2 cast(int) end cut_iiii end

// Proc to copy two string
inline proc copy_ss int ptr int ptr -- int ptr int ptr in peek a b c d in a b c d cut_ss end end

// Proc to paste two string
inline proc paste_ss -- int ptr int ptr in paste_iiii let n1 s1 n2 s2 in n1 s1 cast(ptr) n2 s2 cast(ptr) end end

// Procs to cut strings
inline proc cut_is int int ptr in let c i p in c cast(int) i p cast(int) cut_iii end end
inline proc cut_si int ptr int in let i p c in i p cast(int) c cast(int) cut_iii end end
inline proc cut_bs bool int ptr in let c i p in c cast(int) i p cast(int) cut_iii end end
inline proc cut_sb int ptr bool in let i p c in i p cast(int) c cast(int) cut_iii end end
inline proc cut_ps ptr int ptr in let c i p in c cast(int) i p cast(int) cut_iii end end
inline proc cut_sp int ptr ptr in let i p c in i p cast(int) c cast(int) cut_iii end end
inline proc cut_as addr int ptr in let c i p in c cast(int) i p cast(int) cut_iii end end
inline proc cut_sa int ptr addr in let i p c in i p cast(int) c cast(int) cut_iii end end

// Procs to copy strings
inline proc copy_is int int ptr -- int int ptr in peek c i p in c cast(int) i p cast(int) cut_iii end end
inline proc copy_si int ptr int -- int ptr int in peek i p c in i p cast(int) c cast(int) cut_iii end end
inline proc copy_bs bool int ptr -- bool int ptr in peek c i p in c cast(int) i p cast(int) cut_iii end end
inline proc copy_sb int ptr bool -- int ptr bool in peek i p c in i p cast(int) c cast(int) cut_iii end end
inline proc copy_ps ptr int ptr -- ptr int ptr in peek c i p in c cast(int) i p cast(int) cut_iii end end
inline proc copy_sp int ptr ptr -- int ptr ptr in peek i p c in i p cast(int) c cast(int) cut_iii end end
inline proc copy_as addr int ptr -- addr int ptr in peek c i p in c cast(int) i p cast(int) cut_iii end end
inline proc copy_sa int ptr addr -- int ptr addr in peek i p c in i p cast(int) c cast(int) cut_iii end end

// Procs to paste strings
inline proc paste_is -- int int ptr in paste_iii let c i p in c cast(int) i p cast(ptr) end end
inline proc paste_si -- int ptr int in paste_iii let i p c in i p cast(ptr) c cast(int) end end
inline proc paste_bs -- bool int ptr in paste_iii let c i p in c cast(bool) i p cast(ptr) end end
inline proc paste_sb -- int ptr bool in paste_iii let i p c in i p cast(ptr) c cast(bool) end end
inline proc paste_ps -- ptr int ptr in paste_iii let c i p in c cast(ptr) i p cast(ptr) end end
inline proc paste_sp -- int ptr ptr in paste_iii let i p c in i p cast(ptr) c cast(ptr) end end
inline proc paste_as -- addr int ptr in paste_iii let c i p in c cast(addr) i p cast(ptr) end end
inline proc paste_sa -- int ptr addr in paste_iii let i p c in i p cast(ptr) c cast(addr) end end
