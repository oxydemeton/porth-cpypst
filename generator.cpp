#include <fstream>
#include <iostream>

//Default Output Filename
char default_outfile[] = "cpypst.porth";

//Struct for a literal in porth
struct porth_types_str {
	char name[5];// 5 is the size of "addr" and "bool"
	char ch;
};
//List of literals of porth.
constexpr size_t porth_types_count = 4;
constexpr porth_types_str porth_types[porth_types_count]{
	{ "int", 'i' },
	{ "bool", 'b' },
	{ "ptr", 'p' },
	{ "addr", 'a' }
};

// Stuff on top of the porth file. //TODO: Consts will be splitted into genneration one time
namespace header{
	constexpr char inc[] = "include \"core.porth\"";
	constexpr char con_buf_size[] = "const cpypst_buf_size 8 4 * end";
	constexpr char mem_buf[] = "memory cpypst_buf cpypst_buf_size end";
	constexpr char proc_free[] = "proc cpypst_free in 0 while dup cpypst_buf_size < do dup cpypst_buf cast(int) + cast(ptr) 0 swap !8 1 + end drop end";
	constexpr char proc_buf_byte[] = "inline proc cpypst_buf_byte int -- ptr in cpypst_buf swap ptr+ end";
	constexpr char con_size_mul[] = "const sizeof(int)2 sizeof(int) 2 * end\nconst sizeof(int)3 sizeof(int) 3 * end\nconst sizeof(bool)2 sizeof(bool) 2 * end\nconst sizeof(bool)3 sizeof(bool) 3 * end\nconst sizeof(ptr)2 sizeof(ptr) 2 * end\nconst sizeof(ptr)3 sizeof(ptr) 3 * end\nconst sizeof(addr)2 sizeof(addr) 2 * end\nconst sizeof(addr)3 sizeof(addr) 3 * end";
	constexpr char unimacro[] = "// %%%cut_1; cast(int) cut_i\n// %%%copy_1; dup cast(int) cut_i\n// %%%cut_2; swap cast(int) swap cast(int) cut_ii\n// %%%copy_2; over cast(int) over cast(int) cut_ii\n// %%%cut_3; let a b c in a cast(int) b cast(int) c cast(int) cut_iii end\n// %%%copy_3; let a b c in a cast(int) b cast(int) c cast(int) cut_iii  end\n// %%%cut_4; let a b c d in a cast(int) b cast(int) c cast(int) d cast(int) cut_iiii end\n// %%%copy_4; peek a b c d in a cast(int) b cast(int) c cast(int) d cast(int) cut_iiii end\n";
	void con_size_add(std::ofstream* f, const porth_types_str t[], const size_t array_size){
		for(size_t i = 0; i < array_size; i++){
			for(size_t h = 0; h < array_size; h++){
				*f << "const sizeof(" << t[i].name << "+" << t[h].name << ") sizeof(" << t[i].name << ") sizeof(" << t[h].name << ") + end\n";
			}
		}
	}
};



//Constant procs for three or four values
constexpr char cut_iii[] = "proc cut_iii int int int in cpypst_free let a b c in a cpypst_buf !int  b cpypst_buf sizeof(int) ptr+ !int c cpypst_buf sizeof(int)2 ptr+ !int end end\n";
constexpr char cut_iiii[] = "proc cut_iiii int int int int in cpypst_free let a b c d in a cpypst_buf !int b cpypst_buf sizeof(int) ptr+ !int c cpypst_buf sizeof(int)2 ptr+ !int d cpypst_buf  sizeof(int)3 ptr+ !int end end\n";
constexpr char copy_iii[] = "inline proc copy_iii int int int -- int int int in peek a b c in a b c cut_iii end end";
constexpr char copy_iiii[] = "inline proc copy_iiii int int int int -- int int int int in peek a b c d in a b c d cut_iiii end end\n";
constexpr char paste_iii[] = "inline proc paste_iii -- int int int in paste_ii sizeof(int)2 cpypst_buf_byte @int end\n";
constexpr char paste_iiii[] = "proc paste_iiii -- int int int int in cpypst_buf @int sizeof(int) cpypst_buf_byte @int sizeof(int)2 cpypst_buf_byte @int sizeof(int)3 cpypst_buf_byte @int end\n";

//Constant procs for Strings
constexpr char cut_s[] = "inline proc cut_s int ptr in cut_ip end\n";
constexpr char copy_s[] = "inline proc copy_s int ptr -- int ptr in over over cut_s end\n";
constexpr char paste_s[] = "inline proc paste_s -- int ptr in paste_ip end\n";

constexpr char cut_ss[] = "inline proc cut_ss int ptr int ptr in let n1 s1 n2 s2 in n1 s1 cast(int) n2 s2 cast(int) end cut_iiii end\n";
constexpr char copy_ss[] = "inline proc copy_ss int ptr int ptr -- int ptr int ptr in peek a b c d in a b c d cut_ss end end\n";
constexpr char paste_ss[] = "inline proc paste_ss -- int ptr int ptr in paste_iiii let n1 s1 n2 s2 in n1 s1 cast(ptr) n2 s2 cast(ptr) end end\n";


//Namespace for Functions which gennerate code and write them into the file
namespace write {
	void header(std::ofstream* f, const porth_types_str t[], const size_t array_size) {
		*f << header::inc << "\n\n" ;
		*f << "//Buffer Allocation\n" << header::con_buf_size << "\n";
		*f << header::mem_buf << "\n\n";
		*f << "//Proc to clear the buffer.\n" << header::proc_free << "\n";
		*f << header::proc_buf_byte << "\n\n";
		*f << "//Consts for multiplication of sizeof()\n" << header::con_size_mul << "\n\n";
		header::con_size_add(f, t, array_size);
	}
	//Gennerates and saves procs which cut one Element 
	void cut_x(std::ofstream* f, const porth_types_str t[], const size_t array_size) {
		for (size_t i = 0; i < array_size; i++) {
			*f << "proc cut_" << t[i].ch << ' ' << t[i].name << " in cpypst_free cpypst_buf !" << t[i].name << " end\n";
		}
	}
	//Gennerates and saves procs which cut two Elements
	void cut_xx(std::ofstream* f, const porth_types_str t[], const size_t array_size) {
		for (size_t i = 0; i < array_size; i++) {
			for (size_t h = 0; h < array_size; h++) {
				*f << "proc cut_" << t[i].ch << t[h].ch << ' ' << t[i].name << ' ' << t[h].name << " in cpypst_free swap cpypst_buf !" << t[i].name << " cpypst_buf sizeof(" << t[i].name << ") ptr+ !" << t[h].name << " end\n";
			}
		}
	}
	//Gennerates and saves procs which copy one Element
	void copy_x(std::ofstream* f, const porth_types_str t[], const size_t array_size) {
		for (size_t i = 0; i < array_size; i++) {
			*f << "inline proc copy_" <<t[i].ch << ' ' << t[i].name << " -- " << t[i].name << " in dup cut_" << t[i].ch << " end\n";
		}
	}
	//Gennerates and saves procs which copy two Elements
	void copy_xx(std::ofstream* f, const porth_types_str t[], const size_t array_size) {
		for (size_t i = 0; i < array_size; i++) {
			for (size_t h = 0; h < array_size; h++) {
				*f << "inline proc copy_" << t[i].ch << t[h].ch << ' ' << t[i].name << ' ' << t[h].name << " -- " << t[i].name << ' ' << t[h].name << " in over over cut_" << t[i].ch << t[h].ch << " end\n";
			}
		}
	}
	//Gennerates and saves procs which paste one Element
	void paste_x(std::ofstream* f, const porth_types_str t[], const size_t array_size) {
		for (size_t i = 0; i < array_size; i++) {
			*f << "inline proc paste_" << t[i].ch << " -- " << t[i].name << " in cpypst_buf @" << t[i].name << " end\n";
		}
	}
	//Gennerates ans saves procs which paste two Elements
	void paste_xx(std::ofstream* f, const porth_types_str t[], const size_t array_size) {
		for (size_t i = 0; i < array_size; i++) {
			for (size_t h = 0; h < array_size; h++) {
				*f << "inline proc paste_" << t[i].ch << t[h].ch << " -- " << t[i].name << ' ' << t[h].name << " in paste_" << t[i].ch << " sizeof(" << t[i].name << ")  cpypst_buf_byte @" << t[h].name << " end\n";
			}
		}
	}

	void cut_xs(std::ofstream* f, const porth_types_str t[], const size_t array_size){
		for(size_t i = 0; i < array_size; i++){
			*f << "inline proc cut_" << t[i].ch << "s " << t[i].name << " int ptr in let c i p in c cast(int) i p cast(int) cut_iii end end\n";
			*f << "inline proc cut_s" << t[i].ch << " int ptr " << t[i].name << " in let i p c in i p cast(int) c cast(int) cut_iii end end\n";
		}
	}
	void copy_xs(std::ofstream* f, const porth_types_str t[], const size_t array_size){
		for(size_t i = 0; i < array_size; i++){
			*f << "inline proc copy_" << t[i].ch << "s " << t[i].name << " int ptr -- " <<  t[i].name << " int ptr in peek c i p in c cast(int) i p cast(int) cut_iii end end\n";
			*f << "inline proc copy_s" << t[i].ch << " int ptr " << t[i].name << " -- int ptr " << t[i].name << " in peek i p c in i p cast(int) c cast(int) cut_iii end end\n";
		}
	}
	void paste_xs(std::ofstream* f, const porth_types_str t[], const size_t array_size){
		for(size_t i = 0; i < array_size; i++){
			*f << "inline proc paste_" << t[i].ch << "s -- " << t[i].name << " int ptr in paste_iii let c i p in c cast(" << t[i].name << ") i p cast(ptr) end end\n";
			*f << "inline proc paste_s" << t[i].ch << " -- int ptr " << t[i].name << " in paste_iii let i p c in i p cast(ptr) c cast(" << t[i].name << ") end end\n";
		}
	}
}

////////////////		MAIN	FUNCTION		////////////
int main(int argc, char** argv) {
	char* outfile;
	if(argc <= 1){
		outfile = default_outfile;
	}else{
		outfile = argv[1];
	}

	std::ofstream f;
	try{
		f.open(outfile);
	}catch (std::system_error& e) {
    	std::cerr << e.code().message() << std::endl;
		exit(1);
	}


	write::header(&f, porth_types, porth_types_count);
	
	f << "\n\n// Macros wich can be dynamicly included by using unimacro.py from https://gitlab.com/oxydemeton/unimacros\n\n" << header::unimacro;

	f << "\n//Procs to cut a single Element\n";
	write::cut_x(&f, porth_types, porth_types_count);
	
	f << "\n //Procs to cut two Elements\n";
	
	write::cut_xx(&f, porth_types, porth_types_count);
	f << "\n// Proc to cut 3 ints\n" << cut_iii;
	f << "\n// Proc to cut 4 ints\n" << cut_iiii;
	
	f << "\n//Procs to copy a single Element\n";
	write::copy_x(&f, porth_types, porth_types_count);
	f << "\n//Proc to copy two Elements\n";
	write::copy_xx(&f, porth_types, porth_types_count);
	
	f << "\n// Proc to copy 3 ints\n" << copy_iii;
	f << "\n// Proc to copy 4 ints\n" << copy_iiii;

	f << "\n //Procs to paste a single Element\n";
	write::paste_x(&f, porth_types, porth_types_count);

	f << "\n //Procs to paste two Element\n";
	write::paste_xx(&f, porth_types, porth_types_count);

	f << "\n// Proc to paste 3 ints\n" << paste_iii;
	f << "\n// Proc to paste 4 ints\n" << paste_iiii;

	f << "\n// Proc to cut a string\n" << cut_s;
	f << "\n// Proc to copy a string\n" << copy_s;
	f << "\n// Proc to paste a string\n" << paste_s;

	f << "\n// Proc to cut two string\n" << cut_ss;
	f << "\n// Proc to copy two string\n" << copy_ss;
	f << "\n// Proc to paste two string\n" << paste_ss;


	f << "\n// Procs to cut strings\n";
	write::cut_xs(&f, porth_types, porth_types_count);
	f << "\n// Procs to copy strings\n";
	write::copy_xs(&f, porth_types, porth_types_count);
	f << "\n// Procs to paste strings\n";
	write::paste_xs(&f, porth_types, porth_types_count);

	f.close();
}
