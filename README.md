# [porth](https://gitlab.com/tsoding/porth) cpypst

## Overview
Copypaste/cpypst is a single [porth](https://gitlab.com/tsoding/porth) file which provides procedures to copy, cut and paste up to four elements to/from a specific buffer.
# Install
Copy the cpypst.porth file into your projekt.
The File needs [core.porth](https://gitlab.com/tsoding/porth/-/blob/master/std/core.porth) contained in a default porth installation.

# How to use
- `Cut` removes up to top four elements from the stack and clones them into a global buffer.

- `Copy` does the same as cut but it doesn't remove those elements from the stack.

- `Paste` clones up to four elements from the buffer onto the stack in the same order as when they have copied.  

- `cpypst_free` clears the Buffer

## Macros
- To use the macros use [unimacro](https://gitlab.com/oxydemeton/unimacros/-/blob/main/unimacro.py)
- To applie them in a file use
```console
$ python3 unimacro.py cpypst.porth [File to applie them]... -o [Seperat output file](optional)
```
- With macros you can cut/copy a non defined combination of data. Also 4 pointer or special combinations.
- Use following macros

| File in code  | What it does                                                                      |
| ---           | ---                                                                               |
| `?cut_1`      | cuts the top stack element(no type checking)                                      |
| `?copy_1`     | copies the top stack element(no type checking/changing)                           |
| `?cut_2`      | cuts the top 2 elements from the stack (also no type checking, any combination)   |
| `?copy_2`     | copies the top 2 elements from the stack (anycombination, without chaninging the types) |
| ...           | ...|
| `copy_4`      | copies 4 elements from the top of the stack (any combination, without changing the types) |

Each time you copy or cut elements the buffer will be cleared but you can paste as often as you want.
## Supported Types

Basicly Every type is supported but used in the procedures with single characters:
| Type                    |  char |
| ---                     | ---
|       `int`             |  i  |
|       `bool`            |  b  |
|       `ptr`             |  p  |
|       `addr`            |  a  |
|porth `string`(int, ptr) |  s  |

##### example: 
~~~
proc main in
    10
    cut_i //Removes one Interger from the stack and copys it into the buffer
    paste_i //Puts the Integer from the buffer back onto the stack
    print
end
~~~

For one or two elements {+ every comibination is implemented +}
##### example:
~~~
    10 true cut_ib      //Cuts int bool
    10 "ABC" copy_is    // Copys an int and a porth string
    paste_pp    // Pastes two pointers onto the stack
    paste_aa    // Pastes the same values as ints onto the stack
~~~
For three or four elements {- Only Integers are implemented. -} So you have to convert each value into an int before beeing able to cut/copy and you have to convert them back after pasting.
example:
~~~
    10 20 30 cut_iii                                            //Cuts three ints
    10 true 20 let i1 b i2 in i1 b cast(int) i2 cut_iii end     //Converts an int, a bool and a int into three ints which can be cutted.
    paste_iii let i1 b i2 in i1 b cast(bool) i2 end             //Puts those values as ints onto the stack but converts the bool back to one.
~~~
Keep in mind: Strings contain an int AND a ptr. So you can only copy two of them but in the procs they are handled like a single Value.
## Order of the Elements
##### Order for this example:
~~~
    10 20 30 copy_iii  print print print  //Copies three ints and prints them from the right on the stack to the left.
    paste_iiii  print print print print        //Tries to paste FOUR ints and prints them from the right on the stack to the left.
~~~
```mermaid
graph TD;
  copy10-->Buf1;
  copy20-->Buf2;
  copy30-->Buf3;
  NULL-->Buf4;
  copy-->print-->paste-->print;
  Buf1-->paste10;
  Buf2-->paste20;
  Buf3-->paste30;
  Buf4-->paste0;
```
##### Output:
~~~
30
20
10
0
30
20
10
~~~

## Code Gennerator
#### Cpp file
The `generator.cpp` contains the code wich will generate the `cpypst.porth`.
If you want to make changes to multiple procedures you should change them here.
#### Makefile
- To use the Makefile open the console in the folder and run `$ make`.
- It generates a new `cpypst.porth` from the `generator.cpp`
- To run the file `g++` is needed.
#### Generate on you own
```console
$ g++ -std=c++11 -o cpypst.out generator.cpp
$ chmod +x cpypst.out
$ ./cpypst.out
```
- If you want to name the .porth file not cpypst.porth you can add the new name to the third line.
- You can use any c++ compile as long it supports c++11 or newer
#### Check the cpypst.porth
##### - Follow the instruction [here](https://gitlab.com/oxydemeton/porth-cpypst/-/tree/main/gen-check).
